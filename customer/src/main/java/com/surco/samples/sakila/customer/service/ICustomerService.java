package com.surco.samples.sakila.customer.service;

import com.surco.samples.sakila.customer.model.CustomerModel;

import java.util.List;
public interface ICustomerService {

    public List<CustomerModel> getAllCustomers();
    public CustomerModel saveCustomer(CustomerModel customerModel);

    public CustomerModel getCustomerById(Short id);

    public CustomerModel updateCustomer(CustomerModel customerModel);

    public void deleteCustomer(Short id);

}
