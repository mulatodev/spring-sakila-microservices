package com.surco.samples.sakila.customer.model;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ICustomerModel extends JpaRepository<CustomerModel, Short> {


}
