package com.surco.samples.sakila.customer.controller;

import com.surco.samples.sakila.customer.service.ICustomerService;
import com.surco.samples.sakila.customer.model.CustomerModel;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private ICustomerService service;

    @GetMapping
    public ResponseEntity<List<CustomerModel>> listCustomers(){

        List<CustomerModel> customers = service.getAllCustomers();
        return ResponseEntity.ok(customers);
    }

    @GetMapping({"/{id}"})
    public ResponseEntity<CustomerModel> getCustomerById(@PathVariable short id){

        CustomerModel customer = service.getCustomerById(id);
        return ResponseEntity.ok(customer);
    }

    @PostMapping
    public ResponseEntity<CustomerModel> updateCustomer(@RequestBody CustomerModel customerRequest) {

        CustomerModel customer = service.saveCustomer(customerRequest);
        return ResponseEntity.status(HttpStatus.CREATED).body(customer);
    }

}
